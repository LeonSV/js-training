export function render(domDescription, container) {
    const elem = document.createElement(domDescription.type);
    if (domDescription.attributes) {
        const elementAttributesList = domDescription.attributes;

        Object.keys(elementAttributesList).map((val) => elem.setAttribute(val, elementAttributesList[val]));
    }

    if (domDescription.children) {
        if (typeof domDescription.children === 'string') {
            elem.textContent = domDescription.children;
        } else {
            elem.innerHTML = domDescription.children;
        }
    }

    container.appendChild(elem);
    return elem;
}

export function createElement(type, [attributes] = null, [children] = []) {
    const node = {
        type: this.type,
        attributes: this.attributes.reduce((obj, item) => obj[item]),
        children: this.children
    };

    return node;
}
