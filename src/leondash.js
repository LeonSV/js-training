module.exports = {
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ARRAY

    areItemsEqual: function(item1, item2) {
        var areEqualObjects = true;
        for (var key in item1) {
            if (item1[key] != item2[key]) {
                areEqualObjects = false;
            }
        }
        return areEqualObjects;
    },

    isKeyInObject: function(objectToCheck, key){
        return (objectToCheck.hasOwnProperty(key) && objectToCheck[key]);
    },

    isArrayPairPartOfObject: function(KeyArrayPair, objectToCheck){
        var keyName = KeyArrayPair[0];
        var keyValue = KeyArrayPair[1];
        return (objectToCheck.hasOwnProperty(keyName) && keyValue == objectToCheck[keyName]);

    },

    elementWithPropertyExists: function(element, property) {
        return element && element.hasOwnProperty(property);
    },

    concat: function(arr) {
         var result = arr;

        //  result = module.exports.reduce.apply(arr, [arr, function(index){
        //     return result.push(index);
        //  }], result);

         for (var arrayIndex = 1; arrayIndex < arguments.length; arrayIndex++) {
            result = result.concat(arguments[arrayIndex]);
         }
         return result;
    },

    map: function(collection, mapper) {
        var result = [];

        // la filter, map si reduce am folosit for-ul nativ
        for (var item in collection){
            
            if (collection[item].hasOwnProperty(mapper)) {
                result.push(collection[item][mapper]);
            } else {
                result.push(mapper.call(this,collection[item]));
            } 
        }

        return result;
    },
    compact: function(arrayToCompact) {
        var result = [];
        var falseValues = [false, null, 0, '', undefined, NaN];

        result = module.exports.filter.apply(arrayToCompact, [arrayToCompact, function(i, val) { return falseValues.indexOf(i) == -1; }])

        // forr (var arrayIndex = 0; arrayIndex < arrayToCompact.length; arrayIndex++) {
        //     if (falseValues.indexOf(arrayToCompact[arrayIndex]) == -1) {
        //         result.push(arrayToCompact[arrayIndex]);
        //     }
        // }

        return result;
    },
    filter: function(collectionToFilter, predicateOrObject ){
        var result = [];
        
        // la filter, map si reduce am folosit for-ul nativ
        for (var collectionKey in collectionToFilter) {
            var currentItem = collectionToFilter[collectionKey];

            var typeOfPredicateOrObject = typeof predicateOrObject;

            switch (typeOfPredicateOrObject) {
                case 'function':
                    if (predicateOrObject.call(this, currentItem)) {
                        result.push(currentItem);
                    }
                    break;
                case 'string':
                    if (module.exports.isKeyInObject(currentItem, predicateOrObject)) {
                        result.push(currentItem);
                    }
                    break;
                default:
                    if (Array.isArray(predicateOrObject)) {
                        if (module.exports.isArrayPairPartOfObject(predicateOrObject, currentItem)) {
                            result.push(currentItem);
                        }  
                    } else {
                        if (module.exports.areItemsEqual(predicateOrObject, currentItem)) {
                            result.push(currentItem);
                        }
                    }
                    break;
            }
        }
        return result;

    },
    reduce: function(coll, iterateeFunction, collector) {
        var result = collector;

        // la filter, map si reduce am folosit for-ul nativ
        for (var currentElementKey in coll){
            result = iterateeFunction.call({},result,coll[currentElementKey], currentElementKey);
        }

        return result;

    },
    flatten: function(arrayToFlatten) {

        /*forr (var arrayIndex = 0;arrayIndex < arrayToFlatten.length;arrayIndex++){
            if (arrayToFlatten[arrayIndex].length > 0) {
                forr (var interiorArrayIndex = 0; interiorArrayIndex < arrayToFlatten[arrayIndex].length; interiorArrayIndex++) {
                    result.push(arrayToFlatten[arrayIndex][interiorArrayIndex]);
                }
            } else result.push(arrayToFlatten[arrayIndex]);
        }

        return result;*/
        var result = arrayToFlatten.reduce(function(result, arrayItem) {
            return result.concat(arrayItem);
        }, [])

        return result;

    },
    uniqBy: function(arrayToUniqfy, uniqCriterion) {
        var result=arrayToUniqfy;

        // aici as fi putut reduce din cod mai bine probabil

        for (var arrayIndex = 0; arrayIndex < result.length; arrayIndex++){

            var currentValue = arrayToUniqfy[arrayIndex];

            if (typeof(uniqCriterion) == 'string'){
                if (module.exports.elementWithPropertyExists(currentValue, uniqCriterion)) { 
                    var initVal = currentValue[uniqCriterion]; 
                }
            }

            for (var indexOfComparisonItem = arrayIndex + 1; indexOfComparisonItem < result.length; indexOfComparisonItem++){
                var valueToBeCompared = arrayToUniqfy[indexOfComparisonItem];
                if (typeof(uniqCriterion) == 'function') {
                    if (uniqCriterion.call({},currentValue) == uniqCriterion.call({}, valueToBeCompared)) { 
                        result[indexOfComparisonItem] = null; 
                    }
                } else {
                    if (module.exports.elementWithPropertyExists(valueToBeCompared, uniqCriterion) && (
                        initVal == valueToBeCompared[uniqCriterion])) { 
                            result[indexOfComparisonItem] = null; 
                    }
                }
            }
        }

        return module.exports.compact(result);
    },

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// FUNCTION

    memoize: function(functionToMemoize,resolverFunction) {
        if (!functionToMemoize.memoCache) { 
            functionToMemoize.memoCache = {};
        } 

        var oldFunction = functionToMemoize;
        
        var memoizedFunction = function() {
            var args = Array.prototype.slice.call(arguments, 0);
            var cacheKey = typeof resolverFunction === 'function' ? resolverFunction.apply({}, args) : arguments[0];
            if (functionToMemoize.memoCache[cacheKey]) {
                return functionToMemoize.memoCache[cacheKey];
            }

            functionToMemoize.memoCache[cacheKey] = oldFunction.apply(oldFunction, arguments);
            return functionToMemoize.memoCache[cacheKey];
        }
        return memoizedFunction;
    },
    negate: function(functionToNegate) {
        var oldFunction = functionToNegate;

        var negatedFunction = function() {
            var originalResult = oldFunction.apply(oldFunction, arguments);
            return !originalResult;
        }
        return negatedFunction;

    },
    curry: function curry(functionToCurry, arity) {
        var oldarguments = [];
        var argslength = arity || functionToCurry.length;
        return function() {
            var args = Array.prototype.slice.call(arguments, 0);
            if (arguments.length === argslength) {
                return functionToCurry.apply(functionToCurry, args);
            } else {
                return curry(function() {
                    var innerArgs = Array.prototype.slice.call(arguments, 0);
                    return functionToCurry.apply(functionToCurry, args.concat(innerArgs));
                }, argslength - arguments.length);
            }
        }
    },

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// OBJECT

    omit: function(source, pathsToOmit) {
        var result = {};

        //result = module.exports.filter.apply(source, [source, function(i, val) { return pathsToOmit.indexOf(i) == -1; }])

        for (var key in source) {
            if (pathsToOmit.indexOf(key) == -1) {
                result[key] = source[key];
            }
        }
        return result;

    },
    toPairs: function(collectionToGroup) {
        var result = [];

        // result = module.exports.filter.apply(collectionToGroup, [collectionToGroup, function(i,val) { 
        //         console.log(val);
        //     if (collectionToGroup.hasOwnProperty(i)){
        //         var pairObject = [];
        //         pairObject.push(i);
        //         pairObject.push(collectionToGroup[i]);
        //         return pairObject;
        //     }
        //     return result;
        // }])

        for (var key in collectionToGroup) {

            if (collectionToGroup.hasOwnProperty(key)){
                var pairObject = [];
                pairObject.push(key);
                pairObject.push(collectionToGroup[key]);
                result.push(pairObject)
            }
        }

        return result;
    },
    get: function getter(objectAtPathDestination, path, defaultVal) {
        if (!Array.isArray(path)) {
            var pathWithoutBrackets = path.replace(/\[/,'.').replace(/\]/, '');
            var pathAsArray = pathWithoutBrackets.split('.');
        } else {
            var pathAsArray = path; 
        }

        if (pathAsArray.length == 0) {
            return objectAtPathDestination;
        } else {
            if (objectAtPathDestination.hasOwnProperty(pathAsArray[0])) {
                objRemainder = objectAtPathDestination[pathAsArray[0]];
                pathAsArray.shift();
                return getter(objRemainder,pathAsArray, defaultVal);
            } else {
                return defaultVal;
            }
        }
    },
    findKey: function(objectWithKey, filterFunction) {
        var result = module.exports.filter(objectWithKey,filterFunction)[0];

        // aici nu cred ca are rost mapare, pt ca trebuie sa se opreasca la prima gasire a cheii
        for (var currentKey in objectWithKey){
            if (objectWithKey[currentKey] == result) {
                return currentKey;
            }
        }
    }
}