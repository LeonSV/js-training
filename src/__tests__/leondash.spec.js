const _ = require('../leondash.js');
function square(n) {
    return n * n;
}

function isEven(n) {
    return n % 2 === 0;
}

function isLargerThanTen(n) {
    return n > 10;
}

function Foo() {
    this.a = 1;
    this.b = 2;
}

function TestToPairs() {
    this.a = 1;
    this.b = this.a;
}

function TestToPairsArg(arg) {
    this.a = 1;
    this.b = this.a;
    this.c = arg;
}

function sumOfThree(a, b, c){
    return a + b + c;
}

describe.only('Concat test suite', function () {
      test.only('Should concat 2 array together', function() {
        var array = [1];
        var other = _.concat(array, 2, [3], [[4]]);
        expect(other.length).toBe(4);
    });

    test('Should return the original array values when no additional values are given', function() {
        var array = [1];
        var other = _.concat(array);
        expect(other.length).toBe(1);
    });

    test('Should concat values when all the items are primitive values', function() {
        var array = [1];
        var other = _.concat(array, 1, 2);
        expect(other.length).toBe(3);
    });

    test('Should concat values when all the items are array values', function() {
        var array = [1];
        var other = _.concat(array, [1, 2]);
        expect(other.length).toBe(3);
    });

    test('Should concat values when all the items are EMPTY array values', function() {
        var array = [1];
        var other = _.concat(array, []);
        expect(other.length).toBe(1);
    });
 });

    ////////////////////////////// ARRAY TESTS     
describe('compact lib suite', function() {   
       
     // -------------------------- DONE - am folosit toEqual
    test('Should remove all falsey values from an array', function() {
        var array = [0, 1, false, 2, '', 3];
        var result = _.compact(array);
        expect(result).toEqual([1, 2, 3]);
    });

    test('Should return same array if no falsey values are in it', function() {
        var array = [6, 77, 33];
        var result = _.compact(array);
        expect(result).toEqual([6, 77, 33]);
    });

    test('Should return empty array if entry array is full of falsey values', function() {
        var array = [0, false, null];
        var result = _.compact(array);
        expect(result).toEqual([]);
    });

});

describe('map lib suite', function() {   

    // -------------------------- DONE - am folosit toEqual
   test('Should return just the user names of objects', function() {
        var users = [
            { 'user': 'barney' },
            { 'user': 'fred' }
        ];

        var result = _.map(users, 'user');
        expect(result).toEqual(['barney', 'fred']);
    });
    // -------------------------- DONE - am folosit toEqual
    test('Should map values from an array to another array', function() {
        var arr = _.map([4, 8], square);
        expect(arr.length).toBe(2);
        expect(arr[0]).toBe(16);
        expect(arr[1]).toBe(64);
    });

    test('Should map values from an array of objects to another array', function() {
        var arr = _.map({ 'a': 4, 'b': 8 }, square);
        expect(arr.length).toBe(2);
        expect(arr[0]).toBe(16);
        expect(arr[1]).toBe(64);
    });
});

describe('filter lib suite', function() {   
    // -------------------------- DONE
    test('Should create an array of values that match the elements of the initial array for which the function returns true', function() {
        var array = [4, 8, 5];


        var result = _.filter(array, isEven);
        expect(result).toEqual([4, 8]);
    });
    // -------------------------- DONE
    test('Should create an array of values that have a property that matches the value returned from the function', function() {
        var users = [
            { 'user': 'barney', 'age': 36, 'active': true },
            { 'user': 'fred',   'age': 40, 'active': false }
        ];

        var result = _.filter(users, function(o) { return !o.active; });

        expect(result[0].user).toBe('fred');
    });
    // -------------------------- DONE
    test('Should create an array of values that have a property that matches the values defined as the predicate ', function() {
        var users = [
            { 'user': 'barney', 'age': 36, 'active': true },
            { 'user': 'fred',   'age': 40, 'active': false }
        ];

        var result = _.filter(users, { 'age': 36, 'active': true });

        expect(result[0].user).toBe('barney');
    });

    test('Should create an array of values that have a property that matches the shorthand matchesProperty iteratee ', function() {
        var users = [
            { 'user': 'barney', 'age': 36, 'active': true },
            { 'user': 'fred',   'age': 40, 'active': false }
        ];

        var result = _.filter(users, ['active', false]);

        expect(result[0].user).toBe('fred');
    });

    test('Should create an array of values that have a property that matches the shorthand property iteratee ', function() {
        var users = [
            { 'user': 'barney', 'age': 36, 'active': true },
            { 'user': 'fred',   'age': 40, 'active': false }
        ];

        var result = _.filter(users, 'active');

        expect(result[0].user).toBe('barney');
    });
});

describe('reduce lib suite', function() {   
    // -------------------------- DONE
    test('Should return the value of the accumulated result of each item in the collection ran through the function', function() {
        var array = [1, 2];

        var result = _.reduce([1, 2], function(sum, n) {
            return sum + n;
            }, 0);

        expect(result).toBe(3);
    });
    // -------------------------- DONE
    test('Should return the collection grouped by the value of the key', function() {
        var obj = { 'a': 1, 'b': 2, 'c': 1 };


        var result = _.reduce({ 'a': 1, 'b': 2, 'c': 1 }, function(result, value, key) {
            (result[value] || (result[value] = [])).push(key);
            return result;
        }, {});

        expect(result).toEqual({ '1': ['a', 'c'], '2': ['b'] });
    });
});

describe('Flatten lib suite', function() {   
    // -------------------------- DONE
    test('Should flatten the array a single level deep', function() {
        var array = [1, [2, [3, [4]], 5]];


        var result = _.flatten(array);
        expect(result).toEqual([1, 2, [3, [4]], 5]);
    });

    test('should return same array when simple array is fed', function() {
        var array = [1, 2, 3];

        var result = _.flatten(array);
        expect(result).toEqual([1, 2, 3]);
    });

    test('should return empty array when empty array is fed', function() {
        var array = [];
        var result = _.flatten(array);
        expect(result).toEqual([]);
    })

    test('should return same array when accidentaly nested more than needed', function() {
        var array = [[1, 2, 3]];
        var result = _.flatten(array);
        expect(result).toEqual([1, 2, 3]);
    })

});

describe('UniqBy lib suite', function() {   
    // -------------------------- DONE
    test('Should create a duplicate free version of an array, generated by the criterion defined to compute the uniqueness', function() {
        var array = [2.1, 1.2, 2.3];


        var result = _.uniqBy(array, Math.floor);
        expect(result).toEqual([2.1, 1.2]);
    });

    test('Should create a duplicate free version of an array, generated by the shorthand property', function() {
        var array = [{ 'x': 1 }, { 'x': 2 }, { 'x': 1 }];


        var result = _.uniqBy(array,'x');
        expect(result).toEqual([{ 'x': 1 }, { 'x': 2 }]);
    });

});

    ////////////////////////////// FUNCTION TESTS
describe.skip('Memoize lib suite', function() {   
    test('should remember the result of a basic power function', function() {
        var memoCache;
        //console.log(_.memoize(square(5), memoCache));
        
        var memoized= _.memoize(square, function(o) {
            return 'square_'+o.id;
        });
        console.log('asd');
        var result = memoized(5);
        console.log('asAAd');
        console.log(result);
        expect(result.toEqual(25));

    });

    test('Should create a function that memoizes the result of fn', function() {
        /*var object = { 'a': 1, 'b': 2 };
        var other = { 'c': 3, 'd': 4 };
        
        var values = _.memoize(_.values);
        values(object);
        // => [1, 2]
        
        values(other);
        // => [3, 4]
        
        object.a = 2;
        values(object);
        // => [1, 2]
        
        // Modify the result cache.
        values.cache.set(object, ['a', 'b']);
        values(object);
        // => ['a', 'b']
        
        // Replace `_.memoize.Cache`.
        _.memoize.Cache = WeakMap;

        expect(result).toBe([{ 'x': 1 }, { 'x': 2 }]);*/
    });
});

describe('Negate lib suite', function() {   
    test('Should return the inverse of the applied function', function() {
        var array = [1, 2, 3, 4, 5, 6];


        var result = _.filter(array, _.negate(isEven));
        expect(result).toEqual([1, 3, 5]);
    });     
    test('Should return numbers smaller than 10', function() {
        var array = [1, 12, 3, 14, 15, 6];


        var result = _.filter(array, _.negate(isLargerThanTen));
        expect(result).toEqual([1, 3, 6]);
    });     
    test('Should return even numbers', function() {
        var array = [1, 2, 3, 4, 5, 6];


        var result = _.filter(array, _.negate(_.negate(isEven)));
        expect(result).toEqual([2, 4, 6]);
    });     
    
});

describe.skip('Curry lib suite', function() {   
    // -------------------------- DONE
    test('Should return the sum when params are in one place', function() {
        /*var abc = function(a, b, c) {
            return [a, b, c];
        };*/

        var threeSum = function(a, b, c) {
            return a + b + c;
        }

        var curried = _.curry(threeSum);

        var result1 = curried(1, 2, 3);

        expect(result1).toBe(6);
    });  
    test('Should return the sum when params are in separate paranthesis', function() {
        var threeSum = function(a, b, c) {
            return a + b + c;
        }

        var curried = _.curry(threeSum);

        var result1 = curried(1,2)(3);

        expect(result1).toBe(6);
    });
    test('Should return the sum when all params are in different paranthesis', function() {
        var abc = function(a, b, c) {
            return [a, b, c];
        };

        var curried = _.curry(abc);

        var result2 = curried(1)(2)(3);

        expect(result2).toBe([1, 2, 3]);
    });  
    test('Should return the curried function', function() {
        var abc = function(a, b, c) {
            return [a, b, c];
        };

        var curried = _.curry(abc);

        var result3 = curried(1, 2, 3);

        expect(result3).toBe([1, 3, 5]);
    });  
    test('Should return the curried function', function() {
        var sum = function(a, b) {
            return a+b;
        };

        var curriedFunc = _.curry(sum);
        var inc = curriedFunc(1);
        var add2 = curriedFunc(2);
        var res = inc(3);
        var result4 = curried(1)(_, 3)(2);

        expect(result4).toBe([1, 3, 5]);
    });      
});

    ////////////////////////////// OBJECT TESTS
describe('Omit lib suite', function() {   

    test('Should return an object with the selected keys ommitted', function() {
        var object = { 'a': 1, 'b': '2', 'c': 3 };

        var result = _.omit(object, ['a', 'c']);

        expect(result).toEqual({ 'b': '2' });
    });  
    test('Should return an object with the selected key ommitted', function() {
        var object = { 'a': 1, 'b': '2', 'a': 3 };

        var result = _.omit(object,'a');

        expect(result).toEqual({ 'b': '2' });
    });  
    test('Should return an empty object', function() {
        var object = { 'a': 1, 'b': '2', 'a': 3 };

        var result = _.omit(object,['a', 'b', 'c']);

        expect(result).toEqual({});
    });  
});

describe('toPairs lib suite', function() {   
    test('Should create an array if own enumerable string keyed-value pairs for object', function() {
        Foo.prototype.c = 3;

        var result = _.toPairs(new Foo);

        expect(result).toEqual([['a', 1], ['b', 2]]);
    });  
    test('Should return empty array if param is empty object', function() {
        var result = _.toPairs({});

        expect(result).toEqual([]);
    });  
    test('Should return empty array if param is null', function() {
        var result = _.toPairs(new TestToPairs);

        expect(result).toEqual([['a', 1], ['b', 1]]);
    });  
    test('Should return  array if fct has param', function() {
        var result = _.toPairs(new TestToPairsArg(3));

        expect(result).toEqual([['a', 1], ['b', 1], ['c', 3]]);
    });  
});

describe('Get lib suite', function() {

    test('Should get the value at the path of the object', function() {
        var object = { 'a': [{ 'b': { 'c': 3 } }] };
        //recursivitate aici
        var result = _.get(object, 'a[0].b.c');

        expect(result).toBe(3);
    });

    test('Should get the value when path is array', function() {
        var object = { 'a': [{ 'b': { 'c': 3 } }] };

        var result = _.get(object, ['a', '0', 'b', 'c']);

        expect(result).toBe(3);
    });     
    // -------------------------- DONE
    test('Should return the default value if path is not found', function() {
        var object = { 'a': [{ 'b': { 'c': 3 } }] };

        var result = _.get(object, 'a.b.c', 'default');

        expect(result).toBe('default');
    });     
});

describe('findKey lib suite', function() { 
    // -------------------------- DONE  
    test('Should get the key of the first element that the fct returns true for', function() {
        var users = {
            'barney':  { 'age': 36, 'active': true },
            'fred':    { 'age': 40, 'active': false },
            'pebbles': { 'age': 1,  'active': true }
        };

        var result = _.findKey(users, function(o) { return o.age < 40; });

        expect(result).toBe('barney');
    }); 
    // -------------------------- DONE  
    test('Should get the key of the first element that matches the object', function() {
        var users = {
            'barney':  { 'age': 36, 'active': true },
            'fred':    { 'age': 40, 'active': false },
            'pebbles': { 'age': 1,  'active': true }
        };

        var result = _.findKey(users, { 'age': 1, 'active': true });

        expect(result).toBe('pebbles');
    }); 
    test('Should get the key of the first element that the fct returns true for', function() {
        var users = {
            'barney':  { 'age': 36, 'active': true },
            'fred':    { 'age': 40, 'active': false },
            'pebbles': { 'age': 1,  'active': true }
        };

        var result = _.findKey(users, ['active', false]);

        expect(result).toBe('fred');
    }); 
    test('Should get the key of the first element that the fct returns true for', function() {
        var users = {
            'barney':  { 'age': 36, 'active': true },
            'fred':    { 'age': 40, 'active': false },
            'pebbles': { 'age': 1,  'active': true }
        };

        var result = _.findKey(users, 'active');

        expect(result).toBe('barney');
    }); 
});