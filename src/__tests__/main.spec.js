import { makeDOM, removeDOM } from '../utils.js';
import { render, createElement } from '../vsdom.js';

describe('DOM tests', () => {
    beforeEach(() => {
        makeDOM();
    });

    afterEach(() => {
        removeDOM();
    });

    it('asdasdasd', () => {
        document.getElementById('app-container').innerHTML = 'asd';

        const innerText = document.getElementById('app-container').innerHTML;

        expect(innerText).toBe('asd');
    });
});

describe('render function that renders a virtual DOM', () => {
    beforeEach(() => {
        makeDOM();
    });

    afterEach(() => {
        removeDOM();
    });

    it('render a simple div inside the app container', () => {
        const div = render({ type: 'div' }, document.getElementById('app-container'));

        const innerH = document.getElementById('app-container').firstChild;
        expect(div).toEqual(innerH);
    });

    it('render an ul inside an element with id', () => {
        const parent = render({ type: 'div' }, document.getElementById('app-container'));
        parent.setAttribute('id', 'test');

        const ul = render({ type: 'ul' }, document.querySelector('#test'));

        const innerH = document.getElementById('test').firstChild;
        expect(ul).toEqual(innerH);
    });

    it('render an element with specified attributes', () => {
        const div = render({
            type: 'div',
            attributes: { name: 'app', id: 'app' }
        }, document.getElementById('app-container'));

        const innerH = document.getElementById('app-container').firstChild;
        expect(div).toEqual(innerH);
    });

    it('render an element with null attributes', () => {
        const div = render({
            type: 'div',
            attributes: null
        }, document.getElementById('app-container'));

        const innerH = document.getElementById('app-container').firstChild;
        expect(div).toEqual(innerH);
    });

    it('render an element with inner text', () => {
        const div = render({
            type: 'div',
            attributes: null,
            children: 'This is the div content as text'
        }, document.getElementById('app-container'));

        const innerH = document.getElementById('app-container').firstChild;
        expect(div).toEqual(innerH);
    });

    it('creates a DOM element object', () => {
        const elem = createElement('div', {'id': 'elem', 'name': 'test-obj'}, ['<span>asd</span>', 'text yo'])
        const result = {
            type: 'div',
            attributes: {
                id: 'elem',
                name: 'test-obj'
            },
            children: ['<span>asd</span>', 'text yo']
        };
        expect(elem).toEqual(result);
    });
});
