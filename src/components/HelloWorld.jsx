import * as React from 'react';
import { Component } from 'react';
export class HelloWorld extends Component {
    render() {
        return (
            <div className="hello-world">
                <h1>Hello World</h1>
            </div>
        );
    }
}
export default HelloWorld;
