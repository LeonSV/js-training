export function makeDOM() {
    const elem = document.createElement('div');
    elem.setAttribute('id', 'app-container');
    document.body.appendChild(elem);
}

export function removeDOM() {
    document.getElementById('app-container').remove();
}
